function addTokens(input, tokens){
    let modified = input
    if(typeof input!=="string")
    {   
        throw new Error("Input should be a string")
    }
    else{
    if(input.length<=6)
    {
        throw new Error("Input should have at least 6 characters")
    }

    if(input.indexOf("...")===-1)
    {
        return input
    }else{
    for (let i = 0; i < tokens.length; i++){
        if (input.indexOf("...") !== -1){
            modified = modified.replace('...', tokens[i])
        } 
    }
    return modified
}



}


}

let input="Subsemnatul ... domiciliat in ... are ...  ani"
let tokens=["Ion" ,  "Bucuresti", "14"]
input=addTokens(input,tokens)
console.log(input)

const app = {
    addTokens: addTokens
}
module.exports = app;